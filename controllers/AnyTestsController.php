<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RequestVue;
use app\models\UploadForm;
use yii\web\UploadedFile;

class AnyTestsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        return $this->render('index');
    }
    /**
     * UserForm action.
     *
     * @return string
     */
    public function actionUserForm()
    {
       
        $this->view->registerCssFile('https://use.fontawesome.com/releases/v5.5.0/css/all.css');
        $this->view->registerCssFile('@web/css/userForm.css');
        return $this->render('userForm');
    }

    public function actionVueOpportunities()
    {
        return $this->render('vueOpportunities');
    }
    

    public function actionVuerequest()
    {
        if(Yii::$app->request->isAjax){
            $name = Yii::$app->request->post('name');
            $thing = Yii::$app->request->post('thing');
            $total = (int) Yii::$app->request->post('total');
            $model = new RequestVue();
            $model->name = $name;
            $model->thing = $thing;
            $model->total = $total;
            $model->save();
        }
        $sendModel = RequestVue::find()->orderBy('id desc')->limit(10)->asArray()->all();
        return json_encode($sendModel);
    }
    
    public function actionVuerequestanswer()
    {
        
        $sendModel = RequestVue::find()->orderBy('id desc')->limit(10)->asArray()->all();
        
        
        return json_encode($sendModel);
        
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionUpload()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
            if ($model->upload()) {
                // file is uploaded successfully
               // return $this->render('viewLinkToPdf', ['model' => $model]);
                return $this->actionTestPdf();
            }
        }

        $this->view->registerCssFile('@web/css/uploadForm.css');

        return $this->render('viewForFiles', ['model' => $model]);
    }

    public function actionTestPdf()
    {
        $content = $this->renderPartial('yulMaket');

        /*echo '<pre>';
        var_dump($someValue->makePdf($content));
        echo '</pre>';
        exit;*/
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        $pdf->cssInline = '.forTitle{text-align:center} .forContent{margin-left: 20px;}';
        $pdf->filename = 'uploads/yurMaket/111.pdf';
        $pdf->destination = \kartik\mpdf\Pdf::DEST_FILE;
        //$pdf->output->dest = 'uploads';
        $pdf->render();

        $model = new UploadForm();
        $this->view->registerCssFile('@web/css/uploadForm.css');
        return $this->render('viewForFiles', ['model' => $model]);
        //return $this->render('testPdf');
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    public function actionList()
    {
        $data = "ttt";
        if(Yii::$app->request->isAjax){
            $id = (int)Yii::$app->request->post('id');
            $data = $id;
            $myProducts = "<option value='0'>" . "Выбрать" . "</option>";
            //$this->option = "<option value='0'>" . AirlinesModule::t('module', 'AIRLINES_PROMPT_FORM') . "</option>";
            /*$products = \app\models\Product::find()
                ->Where('referenceId=:id',[':id' => $id])
                ->orderBy('referenceId')
                ->all();
            foreach ($products as $product){
                $myProducts .= '<option value="' . $product->id . '">' . $product->title . '</option>';
            }selected='selected'*/
            $myProducts .="<option value='cart'>"."Cart"."</option>".
                "<option data-divider='true' disabled>"."____"."</option>".
                "<option value='openmodaloption'>"."Create new list"."</option>";
            $data = $myProducts;
        }
        //return $this->option;
        return $data;
    }

    /**
     * TreeTemplate action.
     *
     * @return string
     */

    public function actionTreeTemplate()
    {

        /*$this->view->registerCssFile('@web/treeTemplate/css/font-awesome.min.css');
        $this->view->registerCssFile('@web/treeTemplate/css/main.css');
        $this->view->registerCssFile('@web/treeTemplate/css/responsive.css');

        $this->view->registerJsFile('@web/treeTemplate/js/jquery.cookie.js', ['position'=>yii\web\View::POS_LOAD]);
        $this->view->registerJsFile('@web/treeTemplate/js/jquery.accordion.js', ['position'=>yii\web\View::POS_LOAD]);
        $this->view->registerJsFile('@web/treeTemplate/js/main.js', ['position'=>yii\web\View::POS_LOAD]);*/

        $this->layout = 'mainTree';
        return $this->render('treeTemplate');
    }

        
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}