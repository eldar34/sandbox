<?php

return [
    /*'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',*/

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',

    'class' => 'yii\db\Connection',
    // 'dsn' => env('DB_DSN', 'sqlite:/path/to/database/file'),
    'dsn' => 'mysql:host=' . env('DB_HOST', '') . ';dbname=' . env('DB_NAME', ''),
    'username' => env('DB_USERNAME', 'MaG'),
    'password' => env('DB_PASSWORD', ''),
    'charset' => 'utf8',
];
