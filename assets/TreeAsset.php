<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TreeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       // 'template/css/bootstrap.min.css',
        'treeTemplate/css/font-awesome.min.css',
        'treeTemplate/css/main.css',
        'treeTemplate/css/responsive.css',
        
    ];
    public $js = [
       // 'template/js/jquery.js',
       // 'template/js/bootstrap.min.js',

        'treeTemplate/js/jquery.cookie.js',
        'treeTemplate/js/jquery.accordion.js',
        'treeTemplate/js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
