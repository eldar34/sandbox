<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TreeTemplate */

$this->title = 'Create Tree Template';
$this->params['breadcrumbs'][] = ['label' => 'Tree Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tree-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
