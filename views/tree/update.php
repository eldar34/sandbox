<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TreeTemplate */

$this->title = 'Update Tree Template: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tree Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tree-template-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
