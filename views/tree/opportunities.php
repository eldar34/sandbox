<?php

use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<?php // Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<div class="container">
  <div class="row">
  	 <div style="display: flex; justify-content: center;">
  	 	<div>
<?php
Modal::begin([
    'header' => '<h2>Create New Item</h2>',
    'options' => ['id'=>'firstSecond'],
    'toggleButton' => ['label' => 'Create', 'class' => 'btn btn-success btn-lg'],
    'footer' => 'Create New Item',
]);

$form = ActiveForm::begin(['layout' => 'horizontal', 'method'=>'post', 'action' => ['create']]);

echo $form->field($model, 'name')->textInput();
echo $form->field($model, 'keywords')->textInput();
echo $form->field($model, 'description')->textInput();

echo $form->field($model, 'parent_id')->textInput();

echo '<div style="text-align:center">';
echo Html::submitButton('Save', ['class' => 'btn btn-primary']);
echo '</div>';


ActiveForm::end();

Modal::end();
?>
			</div>
            <div style="margin-left: 10px;">
<?php
Modal::begin([
    'header' => '<h2>Create New Item</h2>',
    'options' => ['id'=>'firstSecond'],
    'toggleButton' => ['label' => 'Update', 'class' => 'btn btn-primary btn-lg'],
    'footer' => 'Create New Item',
]);

$form = ActiveForm::begin(['layout' => 'horizontal', 'method'=>'post', 'action' => ['create']]);

echo $form->field($model, 'name')->textInput();
echo $form->field($model, 'keywords')->textInput();
echo $form->field($model, 'description')->textInput();

echo $form->field($model, 'parent_id')->textInput();

echo '<div style="text-align:center">';
echo Html::submitButton('Save', ['class' => 'btn btn-primary']);
echo '</div>';


ActiveForm::end();

Modal::end();
?>
      </div>
		</div>
	</div>
</div>