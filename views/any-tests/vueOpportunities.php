<?php
use aki\vue\Vue;
?>
<?php Vue::begin([
    'id' => "vue-app",
    'data' => [
        'message' => "hello",
        'seen' => false,
        'todos' => [
            ['text' => "text"],
            ['text' => "akbar joudi"]
        ],
        'myArray' => []
    ],
    'methods' => [
        'getAnswer' => new yii\web\JsExpression("function(){"
            . "axios.get('/any-tests/vuesend').then((response) => {
                    this.myArray = response.data;
                    this.seen = !this.seen;
                    console.log(response);
                    
                }); "
            . "}"),
        ',reverseMessage' => new yii\web\JsExpression("function() {"
            ."var splitString = this.message.split('');
              var reverseMessage = splitString.reverse();
              var joinMessage = reverseMessage.join('');
              this.message = joinMessage;  
            "
            ."}"),
    ]
]); ?>

    <p>{{ message }}</p>
    <button @click="reverseMessage">Reverse Message</button>
    <button @click="getAnswer">ShowMessage</button>



    <p v-if="seen">Now you see me</p>




<div>
    <div>
    <table class="table-bordered">
        <tr v-for="myval in myArray">
            <td>{{ myval.title }}</td>
            <td>{{ myval.url }}</td>
        </tr>
    </table>
    </div>
</div>


    <ol>
        <li v-for="todo in todos">
            {{ todo.text }}
        </li>
    </ol>

    <p>{{ message }}</p>
    <input v-model="message">




<?php Vue::end(); ?>