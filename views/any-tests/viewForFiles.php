<?php
use yii\widgets\ActiveForm;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
?>

<?php
$files = FileHelper::findFiles('uploads',['only'=>['*.jpg','*.png'], 'recursive'=>false]);
foreach($files as $file){
    FileHelper::unlink($file);
}

?>
<p>Можно загрузить 3 фалйла формата jpg, png</p>


<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <div class="form-group">



<?= $form->field($model, 'imageFiles[]', [
    'template' => '
                       <div class="input-group">
                           
                           {input}
                           <label for="uploadform-imagefiles"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose a file&hellip;</strong></label>
                           
                       </div>
                       {error}
                       {hint}
                      '
])
    ->fileInput(['multiple' => true,
        'accept' => 'image/*',
        'class'=>'inputfile inputfile-6',
        'data-multiple-caption'=>'{count} files selected'
        ]) ?>

    <button class="btn btn-lg btn-success">Загрузить</button>
    </div>

<?php ActiveForm::end() ?>

<?php
$this->registerJs(<<<JS



var inputs = document.querySelectorAll( '.inputfile' );
Array.prototype.forEach.call( inputs, function( input )
{
	var label	 = input.nextElementSibling,
		labelVal = label.innerHTML;
	
	

	input.addEventListener( 'change', function( e )
	{
		var fileName = '';
		console.log(this.files.length);
		if( this.files && this.files.length > 1 ){
			fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			}
		else{
		    var filename2 = e.target.value.replace(/^.*[\\\/]/, '/');
			fileName = filename2.split('/').pop();
		}
		    
		    
			

		if( fileName )
			label.querySelector( 'span' ).innerHTML = fileName;
		else
			label.innerHTML = labelVal;
	});
});


JS
);
?>



