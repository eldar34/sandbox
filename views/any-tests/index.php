<?php
use yii\bootstrap\Modal;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>

<?php
$this->registerJs(<<<JS
    
    
JS
);
?>

<div class="site-index">



    <div class="body-content">
        <?php
            $files = FileHelper::findFiles('uploads',['only'=>['*.csv','*.txt']]);
        $row = 1;
        $dataIsset = [];
        if (( $handle = fopen($files[0], 'r')) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $num = count($data);
                //dx($data);
                //echo "<p> $num полей в строке $row: <br /></p>\n";
                $row++;
                for ($c=0; $c < $num; $c++) {
                    if($row == 2 || $row == 3)
                    {
                        continue;
                    }
                    ArrayHelper::setValue($dataIsset, $row, [
                            '0' => ArrayHelper::getValue($data, 0),
                            '1' => ArrayHelper::getValue($data, 1),
                            '2' => ArrayHelper::getValue($data, 2),
                            '3' => ArrayHelper::getValue($data, 3),
                            '4' => ArrayHelper::getValue($data, 4),
                            '5' => ArrayHelper::getValue($data, 5),
                            '6' => ArrayHelper::getValue($data, 6),
                    ]);
                    //echo $row.' '.$c.' '.$data[$c] . "<br />\n";
                }
            }
            fclose($handle);
        }
        /*foreach ($dataIsset as $val){
            echo $val . "<br />";
        }*/
        $arraySlice = array_slice($dataIsset, 0, 95);
        $finalArray = [];
        $n = 0;
        for($i=0; $i<count($arraySlice); $i++)
            {
                if(!empty($arraySlice[$i][0]))
                {
                    $needleKey = $i;
                    $n=0;
                }
                ArrayHelper::setValue($finalArray, $needleKey.'.'.$n, $arraySlice[$i]);
                $n++;
            }
            $finsArray2 = [];
        foreach ($finalArray as $val){
            if(count($val)>4){
                ArrayHelper::setValue($finsArray2, $val[0][0], [
                    'question_number' => $val[0][1],
                    'question' => $val[0][2],
                    'category' => $val[0][3],
                    'var_a' => $val[1][2],
                    'var_b' => $val[2][2],
                    'var_c' => $val[3][2],
                    'var_d' => $val[4][2],
                ]);
            }else{
                ArrayHelper::setValue($finsArray2, $val[0][0], [
                    'question_number' => $val[0][1],
                    'question' => $val[0][2],
                    'category' => $val[0][3],
                    'var_a' => $val[1][2],
                    'var_b' => $val[2][2],
                    'var_c' => $val[3][2],
                ]);
            }
            //dx($val);
        }
        /*INSERT INTO table_name (column1, column2, column3, ...)
VALUES (value1, value2, value3, ...);*/
        foreach ($finsArray2 as $val)
        {
           if(count($val)>4){
               echo "INSERT INTO test_questions (question, var_a, var_b, var_c, var_d)
            VALUES ('{$val['question']}', '{$val['var_a']}', '{$val['var_b']}', '{$val['var_c']}', '{$val['var_d']}');"."<br>";
           }else{
               echo "INSERT INTO test_questions (question, var_a, var_b, var_c)
            VALUES ('{$val['question']}', '{$val['var_a']}', '{$val['var_b']}', '{$val['var_c']}');"."<br>";
           }
        }
        //dx($finsArray2);
            exit;
        ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">

        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" id="product">

        </div>


    </div>
</div>

<?php
Modal::begin([
    'header' => '<h2>Hello world</h2>',
    'options' => ['id'=>'firstSecond'],
    'footer' => 'Низ окна',
]);
echo 'Say hello...';
Modal::end();
?>

<div id="modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
                <textarea class="form-control" id="message-text"></textarea>



            </div>
            <div class="modal-footer">
                <button type="button" name="button" class="btn btn-default" data-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>
