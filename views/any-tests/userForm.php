<?php
use yii\bootstrap\Modal;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>

<?php
use aki\vue\Vue;
?>
<?php Vue::begin([
    'id' => "vue-app",
    'data' => [
        'message' => "hello",
        'seen' => false,
        'name' => '',
        'thing' => '',
        'total' => '',
        'serverAnswer' => [],
        'todos' => [
            ['text' => "text"],
            ['text' => "akbar joudi"]
        ],
        'myArray' => []
    ],
    'mounted' =>new yii\web\JsExpression("function(){"
        ."
        axios.get('vuerequestanswer').then((response) => {
            console.log(response.data);
                    
                    this.serverAnswer = response.data;                   
                                    
                   
                });
                
                setInterval(
                ()=>{
                axios.get('vuerequestanswer').then((response) => {
                                
                                
                                this.serverAnswer = response.data;
                                
                            });
                    
                }, 10000);
                
         "
        . "}"),
    'methods' => [
        'getAnswer' => new yii\web\JsExpression("function(){"
            . "axios.get('/any-tests/vuesend').then((response) => {
                    this.myArray = response.data;
                    this.seen = !this.seen;
                    console.log(response);
                    
                }); "
            . "}"),
        ',reverseMessage' => new yii\web\JsExpression("function() {"
            ."var splitString = this.message.split('');
              var reverseMessage = splitString.reverse();
              var joinMessage = reverseMessage.join('');
              this.message = joinMessage;  
            "
            ."}"),
        ',submitForm' => new yii\web\JsExpression("function(event) {"
            ."
            let data = {
            name: this.name,
            thing: this.thing,
            total: this.total}
            
            $.ajax({
                url: 'vuerequest',
                data: data,
                type: 'POST',
                success: (res)=>{
                    if(!res) alert('Ошибка!');
                    var dataArray = JSON.parse(res);
                    this.serverAnswer = dataArray;
                    
                    
                },
                error: function(){
                   alert('ERROR');   
                }
            });
            
            
            
            "
            ."}"),
    ]
]); ?>

<div class="container">
    <div class="row main">
        <div style="display: flex">

        <div class="main-login main-center">
            <h5>Sign up once and watch any of our free demos.</h5>
            <form class="testForm" method="post" @submit.prevent="submitForm" action="vuerequest">

                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label">Your Name</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="name" id="name"  v-model="name" placeholder="Enter your Name" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="cols-sm-2 control-label">Your Thing</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="thing" id="email" v-model="thing" placeholder="Enter your Thing" required/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="username" class="cols-sm-2 control-label">Count</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="total" id="username" v-model="total" placeholder="Enter your Count" required/>
                        </div>
                    </div>
                </div>



                <div class="form-group ">
                    <button class="btn btn-default btn-lg btn-block login-button" type="submit" value="submitForm">Register</button>
                </div>

            </form>
        </div>
        <div class="main-center" style="margin-left: 2em;">
            <table class="forUsers">
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Total</th>
                </thead>
                <tbody>
                    <tr v-for="server in serverAnswer">
                        <td><span class="glyphicon">{{ server.id }}</span></td>
                        <td>{{ server.name }}</td>
                        <td>{{ server.thing }}</td>
                        <td>{{ server.total }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>
<?php Vue::end(); ?>

<?php
Modal::begin([
    'header' => '<h2>Hello world</h2>',
    'options' => ['id'=>'firstSecond'],
    'footer' => 'Низ окна',
]);
echo 'Say hello...';
Modal::end();
?>

<div id="modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
                <textarea class="form-control" id="message-text"></textarea>



            </div>
            <div class="modal-footer">
                <button type="button" name="button" class="btn btn-default" data-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs("
    
   
");
?>