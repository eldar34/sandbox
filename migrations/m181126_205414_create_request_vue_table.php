<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request_vue`.
 */
class m181126_205414_create_request_vue_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('request_vue', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'thing' => $this->string(),
            'total' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('request_vue');
    }
}
