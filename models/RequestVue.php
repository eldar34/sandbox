<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "request_vue".
 *
 * @property int $id
 * @property string $name
 * @property string $thing
 * @property int $total
 */
class RequestVue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request_vue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total'], 'integer'],
            [['name', 'thing'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'thing' => 'Thing',
            'total' => 'Total',
        ];
    }
}
